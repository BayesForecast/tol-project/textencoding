//////////////////////////////////////////////////////////////////////////////
// TOL parsing tools
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Set Parsing(Text expression)
//////////////////////////////////////////////////////////////////////////////
{
  Parse(expression)
};
//////////////////////////////////////////////////////////////////////////////
PutDescription(
"Parses a TOL expression and returns the tree as a hierarquical set",
Parsing);
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Text Unparsing(Set tree)
//////////////////////////////////////////////////////////////////////////////
{
  Text _MID = "[Unparsing] ";
//WriteLn(_MID+"TRACE BEGIN");
  Real n = Card(tree);
  Text Unparsingd = Case(
  And(n!=3,n!=4),
  {
    Error(_MID+"A parsing tree branch must have 3 or 4 elements "
    "instead of "<<n+":\n"<<tree+"\n");
    "#UNPARSING_ERROR_001#"
  },
  Grammar(tree[1])!="Text",
  {
    Error(_MID+"The first element of a parsing tree branch must be "
    "a Text containing the name instead of a "<<Grammar(tree[1]));
    "#UNPARSING_ERROR_002#"
  },
  Grammar(tree[2])!="Text",
  {
    Error(_MID+"The second element of a parsing tree branch must be "
    "a Text containing the token type instead of a "<<Grammar(tree[2]));
    "#UNPARSING_ERROR_003#"
  },
  Grammar(tree[3])!="Set",
  {
    Error(_MID+"The third element of a parsing tree branch must be "
    "a Set containing the branches instead of a "<<Grammar(tree[3]));
    "#UNPARSING_ERROR_004#"
  },
  1==1,
  {
  //WriteLn(_MID+"TRACE VALID");
    Text name = tree[1];
    Text type = tree[2];
    Set  branches = tree[3];
  //WriteLn(_MID+"TRACE {'"<<name+"','"<<type+"',"<<Card(branches)+")");
    Text open  = If(n==3,"",tree[4][1]);
    Text close = If(n==3,"",tree[4][2]);
    Text content = Case(
    type=="separator", 
    {
      Real newLine = Or(name==";",open!="(");
      Text break = If(newLine,"\n"," ");
      SetSum(For(1,Card(branches),Text(Real k)
      {
        If(k>1,name,"")<<break<< Unparsing(branches[k])
      }))+break
    },
    type=="binary", 
    {
      Unparsing(branches[1])<< " "<<
      name<< " "<<
      Unparsing(branches[2])
    },
    Or(type=="monary", type=="type"),
    {
      name<< " "<<
      Unparsing(branches[1])
    },
    type=="function",
    {
      Real newLine = name=="SetOfAnything";
      Text break = If(newLine,"\n"," ");
      Text args = SetSum(For(1,Card(branches),Text(Real k)
      {
        If(k>1,",","")<<break<< Unparsing(branches[k])
      }))+break; 
      name<<"("<<args<<")"
    },
    type=="argument",
    {
      If(Sub(name,0,1)=="\"",name+"\"",name)
    },
    1==1,
    {
      Error(_MID+"Uncontrolled type '"<<type+"'");
      "#UNPARSING_ERROR_005#"
    });
    open+content+close
  });
//WriteLn(_MID+"TRACE END");
  ReplaceTable(Unparsingd, [[
  [["#(#","["]],
  [["#)#","]"]],
  [[" #E# ",""]],
  [[" #F# ","\n"]] ]])
};
//////////////////////////////////////////////////////////////////////////////
PutDescription(
"Returns the standarized TOL expression of a parsing tree built with "
"Parsing method",
Unparsing);
//////////////////////////////////////////////////////////////////////////////
